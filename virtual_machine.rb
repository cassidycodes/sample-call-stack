class VirtualMachine
  def initialize()
    @stack = []
  end

  def run(instructions)
    instructions.each do |v|
      if v.is_a?(Integer)
        @stack << v if v.is_a?(Integer)
      else
        @stack <<  @stack.pop(2).sum
      end
    end
    @stack.first
  end
end
