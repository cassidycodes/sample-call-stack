require 'minitest'
require 'minitest/autorun'
require 'shoulda'
require 'pry'
require_relative "../virtual_machine"

describe VirtualMachine do
  before do
    @subject = VirtualMachine.new
    @instructions = [1,2,3,"+","+"]
    @result = 6
  end

  context 'run' do
    should 'return the expected value' do
      assert_equal @result, @subject.run(@instructions)
    end
  end
end